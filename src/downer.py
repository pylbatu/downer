import os
import datetime

daysTilArchive = 120
daysTilDelete = 360
dir_to_search = os.path.curdir
files_to_archive = []
files_to_delete = []


def analyze():
	print('==== Analyzing ===')
	print()
	print('Please wait...')
	for dirpath, dirnames, filenames in os.walk(dir_to_search):
		if dirpath != '.\_archived':
			for file in filenames:
				curpath = os.path.join(dirpath, file)
				file_modified = datetime.datetime.fromtimestamp(
					os.path.getmtime(curpath))
				if datetime.datetime.now() - file_modified > datetime.timedelta(days=daysTilArchive):
					files_to_archive.append(curpath)
		elif dirpath == '.\_archived':
			for file in filenames:
				curpath = os.path.join(dirpath, file)
				file_modified = datetime.datetime.fromtimestamp(
					os.path.getmtime(curpath))
				if datetime.datetime.now() - file_modified > datetime.timedelta(days=daysTilDelete):
					files_to_delete.append(curpath)
	print()
	print('Files to archive: ' + str(len(files_to_archive)))
	print('Archived files to delete: ' + str(len(files_to_delete)))
	print()

def archive():
	print('==== Archiving ===')
	if len(files_to_archive) > 0:
		if not os.path.exists('_archived'):
			os.mkdir('_archived')
			print('_archived folder created!')

	for file in files_to_archive:
		if (os.path.isfile(file)):
			os.rename(file, './_archived/' + file)
			print('moved ' + file + '!')
	print()


def delete_archived():
	print('==== Deleting ===')
	for file in files_to_delete:
		os.remove(file)
		print('deleted ' + file)
	print()


def execute():
	command = input('Please enter ENTER to start folder analysis')
	print()
	analyze()
	if len(files_to_archive) > 0:
		command = input('Do you want to archive old files? [y/n] ').lower()
		if command == 'y':
			print()
			archive()
		else:
			print('Skipping archiving...')
	else:
		print('Nothing to archive.')

	if len(files_to_delete) > 0:
		print()
		command = input('Do you want to delete archived files? [y/n] ').lower()
		if command == 'y':
			command = input(
				'Are you really sure you want to delete the archived files? [y/n] ').lower()
			if command == 'y':
				print()
				delete_archived()
			else:
				print('Skipping deleting...')
		else:
			print('Skipping deleting...')
	else:
		print('Skipping deleting...')


print()
print('=============================')
print('Starting downEr...')
print('A nifty tool to clean up old files rotting in your folder...')
print('=============================')
print()
execute()
print()
print('Thank you for using dowNer')
print('Shutting down...')
print()
command = input('Please enter ENTER to exit')
